import React from "react";
import moment from "moment";
import RangePicker from "../../src";

export default class HeroExample extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            value: moment()
        };
    }

    getRange(state) {
        console.log(state)
    }

    render() {
        return <RangePicker
            onChange={value => this.setState({value})}
            value={this.state.value}
            getRange={this.getRange}
        />
    }
}
