import React from 'react'
import ExampleComponents from './example_components.js'
import HeroExample from './hero_example.js'

export default React.createClass({
  displayName: 'Root',

  render () {
    return (
      <div>
        <div className="hero">
          <div className="hero__content">
            <h1 className="hero__title" style={{direction:'rtl'}}>
              با تشکر از myreal.ir
            </h1>
            <div className="hero__crafted-by">
              <a href="https://myreal.ir" className="hero__crafted-by-link">
                Crafted by <h3 className="logo">Majid joon</h3>
              </a>
            </div>
            <div className="hero__example">
              <HeroExample />
            </div>
          </div>
        </div>

        <a href="https://github.com/mberneti/react-datepicker2/">
          <img className="github-ribbon" src="https://mberneti.github.io/react-datepicker2/images/ribbon.png" alt="Fork me on GitHub" />
        </a>
      </div>
    )
  }
})
