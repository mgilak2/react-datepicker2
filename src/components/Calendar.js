import React, {Component, PropTypes} from "react";
import DaysViewHeading from "./DaysViewHeading";
import DaysOfWeek from "./DaysOfWeek";
import MonthSelector from "./MonthSelector";
import Day from "./Day";
import {getDaysOfMonth} from "../utils/moment-helper";
import moment from "moment-jalaali";
import {defaultStyles} from "./DefaultStyles";
import {toPersianDigits} from "./DatePicker";

export class Calendar extends Component {
    static propTypes = {
        min: PropTypes.object,
        max: PropTypes.object,
        nextMonthDate: PropTypes.object,
        styles: PropTypes.object,
        defaultMonth: PropTypes.object,
        onSelect: PropTypes.func,
        syncSelectedDay: PropTypes.func,
        onClickOutside: PropTypes.func,
        containerProps: PropTypes.object,
        isGregorian: PropTypes.bool,
        max_month: PropTypes.bool,
        min_month: PropTypes.bool,
        selectedDay: PropTypes.array
    };

    static childContextTypes = {
        setCalendarMode: PropTypes.func.isRequired,
        setMonth: PropTypes.func.isRequired,
        setType: PropTypes.func.isRequired,
    };

    static defaultProps = {
        styles: defaultStyles,
        containerProps: {},
        isGregorian: true,
        selectedDay: [],
        max_month: false,
        min_month: false,
    };

    state = {
        month: this.props.defaultMonth || moment(this.props.min || this.props.defaultMonth),
        selectedDay: this.props.selectedDay,
        mode: 'days',
        isGregorian: this.props.isGregorian,
        hoveredDay: null,
    };

    days = null;
    lastRenderedMonth = null;
    inputRef;

    componentDidMount() {
        this.inputRef.value = this.props.start_at;
    }

    getChildContext() {
        return {
            setCalendarMode: this.setMode.bind(this),
            setMonth: this.setMonth.bind(this),
            setType: this.setMonth.bind(this)
        };
    }

    componentWillReceiveProps({selectedDay, defaultMonth, min}) {
        if (this.props.selectedDay !== selectedDay) {
            this.selectDay(selectedDay);
        } else if (defaultMonth && this.props.defaultMonth !== defaultMonth && this.state.month === this.props.defaultMonth) {
            this.setMonth(defaultMonth);
        } else if (min && this.props.min !== min && this.state.month.isSame(this.props.min)) {
            this.setMonth(min.clone());
        }
    }

    month_order(selectedDay) {
        if (selectedDay.length === 0)
            return null;

        if (selectedDay.length === 1)
            return selectedDay[0];

        if (selectedDay.length === 2) {
            if (selectedDay[0].isSameOrAfter(selectedDay[1])) {
                if (this.props.max_month) {
                    return selectedDay[0];
                } else {
                    return selectedDay[1];
                }
            } else {
                if (this.props.max_month) {
                    return selectedDay[1];
                } else {
                    return selectedDay[0];
                }
            }
        }

        return null;
    }

    setMode(mode) {
        this.setState({mode});
    }

    setMonth(month) {
        this.setState({month});
    }

    setType(type) {
        this.setState({type});
    }

    sameDay(selectedDay, givenDay, isGregorian) {
        for (let i = 0; i < selectedDay.length; i++) {
            const format = isGregorian ? 'YYYYMMDD' : 'jYYYYjMMjDD';
            if (givenDay.format(format) === selectedDay[i].format(format))
                return true;
        }

        return false
    }

    selectDay(givenDay) {
        const {month, isGregorian, selectedDay} = this.state;
        const {syncSelectedDay} = this.props;
        const yearMonthFormat = isGregorian ? 'YYYYMM' : 'jYYYYjMM';

        if (this.sameDay(selectedDay, givenDay, isGregorian)) {
            const format = isGregorian ? 'YYYYMMDD' : 'jYYYYjMMjDD';
            const days = selectedDay.filter(day => {
                if (givenDay.format(format) !== day.format(format))
                    return day;
            });
            this.setState({selectedDay: days});
            syncSelectedDay({selectedDay: days});
        } else {
            if (selectedDay.length === 2) {
                this.setState({selectedDay: [givenDay]});
                syncSelectedDay({selectedDay: [givenDay]});
                return;
            }

            if (selectedDay.length === 1 && givenDay.isAfter(selectedDay[0])) {
                this.setState({selectedDay: [...selectedDay, givenDay]});
                syncSelectedDay({selectedDay: [...selectedDay, givenDay]});
            } else {
                this.setState({selectedDay: [givenDay]});
                syncSelectedDay({selectedDay: [givenDay]});
            }
        }

        if (givenDay.format(yearMonthFormat) !== month.format(yearMonthFormat)) {
            this.setState({month: givenDay});
        }
    }

    handleClickOnDay = selectedDay => {
        if (selectedDay.startOf('day').isBefore(moment().startOf('day'))) {
            return;
        }

        const {onSelect} = this.props;
        this.selectDay(selectedDay);
        if (onSelect) {
            onSelect(selectedDay);
        }
    };

    handleMouseOverOnDay(hoveredDay) {
        const {selectedDay} = this.state;
        if (!!selectedDay.length) {
            this.setState({hoveredDay: hoveredDay});
        }
    };

    renderMonthSelector() {
        const {month, isGregorian} = this.state;
        const {styles} = this.props;
        return (<MonthSelector styles={styles} isGregorian={isGregorian} selectedMonth={month}/>);
    }

    selectedDay(selectedDay, day) {
        let selected = false;
        for (let i = 0; i < selectedDay.length; i++) {
            if (selectedDay[i].isSame(day, 'day')) {
                selected = true;
                break;
            }
        }

        return selected;
    }

    hover(selectedDay, hoveredDay, day, selected) {
        let hover = false;
        if (selectedDay.length === 1 && !!hoveredDay) {
            console.log("here we die");
            if (hoveredDay.isSameOrAfter(selectedDay[0]) && day.isBetween(selectedDay[0], hoveredDay, null, '(]')) {
                hover = true;
            }
        } else if (selectedDay.length === 2) {
            if (selectedDay[1].isSameOrAfter(selectedDay[0]) && day.isBetween(selectedDay[0], selectedDay[1], null, '()')) {
                hover = true;
            } else if (day.isBetween(selectedDay[1], selectedDay[0], null, '()')) {
                hover = true;
            }
        }

        if (selected)
            return false;

        return hover;
    }

    renderDays() {
        const {month, selectedDay, hoveredDay, isGregorian} = this.state;
        const {children, min, max, styles, max_month, min_month, nextMonth, prevMonth} = this.props;
        let days;

        if (this.lastRenderedMonth === month) {
            days = this.days;
        } else {
            days = getDaysOfMonth(month, isGregorian);
            this.days = days;
            this.lastRenderedMonth = month;
        }

        const monthFormat = isGregorian ? 'MM' : 'jMM';
        const dateFormat = isGregorian ? 'YYYYMMDD' : 'jYYYYjMMjDD';

        return (
            <div>
                {children}
                <DaysViewHeading isGregorian={isGregorian} max_month={max_month} min_month={min_month}
                                 nextMonth={nextMonth} prevMonth={prevMonth} styles={styles} month={month}/>
                <DaysOfWeek styles={styles} isGregorian={isGregorian}/>
                <div className={styles.dayPickerContainer}>
                    {
                        days.map(day => {
                            const isCurrentMonth = day.format(monthFormat) === month.format(monthFormat);
                            const disabled = (min ? day.isBefore(min) : false) || (max ? day.isAfter(max) : false);
                            let selected = this.selectedDay(selectedDay, day);
                            let hover = this.hover(selectedDay, hoveredDay, day, selected);

                            return (
                                <Day
                                    isGregorian={isGregorian}
                                    key={day.format(dateFormat)}
                                    onClick={this.handleClickOnDay}
                                    onMouseOver={this.handleMouseOverOnDay.bind(this)}
                                    day={day}
                                    disabled={disabled}
                                    selected={selected}
                                    isCurrentMonth={isCurrentMonth}
                                    styles={styles}
                                    hovered={hover}
                                />
                            );
                        })
                    }
                </div>
            </div>
        );
    }

    onMouseLeave() {
        if (this.state.selectedDay.length === 1 && !this.props.min_month) {
            this.setState({
                hoveredDay: null
            })
        }
    }

        handleKeyPress = (event) => {
            const {start_at_onChange} = this.props;

            if (event.key === 'Enter') {
                start_at_onChange(event);
            }
        };

    render() {
        const {styles, className} = this.props;
        const {isGregorian} = this.state;
        const jalaaliClassName = isGregorian ? '' : 'jalaali ';

        return (
            <div className={styles.calendarContainer + ' ' + jalaaliClassName + className}
                 onMouseLeave={this.onMouseLeave.bind(this)}>
                <div className="start-filter">
                    <span>از تاریخ</span>
                    <input type="text"  maxLength="10"
                           ref={(input) => { this.inputRef = input }}
                           onKeyPress={this.handleKeyPress} style={{direction: "ltr"}}/>
                </div>
                {this.renderDays()}
            </div>
        );
    }
}

export default Calendar;
