import React, {Component, PropTypes} from "react";
import moment from "moment-jalaali";
import Calendar from "./Calendar";
import MyTimePicker from "./CustomTimePicker";
import {persianNumber} from "../utils/persian";

export const outsideClickIgnoreClass = 'ignore--click--outside';
moment.loadPersian();

export function toPersianDigits(value) {
    const numbers = {
        '0': '۰',
        '1': '۱',
        '2': '۲',
        '3': '۳',
        '4': '۴',
        '5': '۵',
        '6': '۶',
        '7': '۷',
        '8': '۸',
        '9': '۹'
    };

    return value.replace(/([0-9])/g, function (number) {
        return numbers[number];
    });
}

export function toEnglishDigits(value) {
    let numbers = {
        '۰': '0',
        '۱': '1',
        '۲': '2',
        '۳': '3',
        '۴': '4',
        '۵': '5',
        '۶': '6',
        '۷': '7',
        '۸': '8',
        '۹': '9'
    };

    return value.replace(/([۰-۹])/g, function (number) {
        return numbers[number];
    });
}


class DatePicker extends Component {
    static propTypes = {
        value: PropTypes.object,
        defaultValue: PropTypes.object,
        onChange: PropTypes.func,
        onFocus: PropTypes.func,
        onBlur: PropTypes.func,
        children: PropTypes.node,
        min: PropTypes.object,
        max: PropTypes.object,
        defaultMonth: PropTypes.object,
        inputFormat: PropTypes.string,
        removable: PropTypes.bool,
        styles: PropTypes.object,
        calendarStyles: PropTypes.object,
        calendarContainerProps: PropTypes.object,
        isGregorian: PropTypes.bool,// jalaali or gregorian
        timePicker: PropTypes.bool,
        min_month: PropTypes.bool,
        max_month: PropTypes.bool,
    };

    static defaultProps = {
        styles: undefined,
        calendarContainerProps: {},
        max_month: false,
        min_month: false,
        isGregorian: true,
        timePicker: true
    };

    state = {
        isOpen: false,
        momentValue: this.props.defaultValue || null,
        inputValue: this.getValue(this.props.defaultValue, this.props.isGregorian, this.props.timePicker),
        inputFormat: this.props.inputFormat || this.getInputFormat(this.props.isGregorian, this.props.timePicker),
        isGregorian: this.props.isGregorian,
        timePicker: this.props.timePicker,
        timePickerComponent: this.props.timePicker ? MyTimePicker : undefined,
        start_at: this.props.max_month ? persianNumber(this.props.defaultMonth.startOf('jMonth').format('jYYYY/jMM/jDD')) :
            persianNumber(this.props.defaultMonth.format('jYYYY/jMM/jDD'))
    };

    getInputFormat(isGregorian, timePicker) {
        if (timePicker)
            return isGregorian ? 'YYYY/M/D hh:mm A' : 'jYYYY/jM/jD hh:mm A';
        return isGregorian ? 'YYYY/M/D' : 'jYYYY/jM/jD';
    }

    getValue(inputValue, isGregorian, timePicker) {
        if (!inputValue)
            return '';
        const inputFormat = this.getInputFormat(isGregorian, timePicker);
        return isGregorian ? inputValue.locale('es').format(inputFormat) : inputValue.locale('fa').format(inputFormat);
    }

    componentWillMount() {
        if (this.props.value) {
            this.setMomentValue(this.props.value);
        }
    }

    componentWillReceiveProps(nextProps) {
        if ('value' in nextProps && nextProps.value !== this.props.value) {
            this.setMomentValue(nextProps.value);
        }

        if ('isGregorian' in nextProps && nextProps.isGregorian !== this.props.isGregorian) {
            const inputFormat = nextProps.isGregorian ? 'YYYY/M/D hh:mm A' : 'jYYYY/jM/jD hh:mm A';

            this.setState({
                isGregorian: nextProps.isGregorian,
                inputValue: this.getValue(nextProps.value, nextProps.isGregorian, nextProps.timePicker),
                inputFormat: inputFormat
            });
        }

        if ('timePicker' in nextProps && nextProps.timePicker !== this.props.timePicker) {
            this.setState({
                timePicker: nextProps.timePicker,
                timePickerComponent: this.props.timePicker ? MyTimePicker : undefined
            });
        }

    }

    setMomentValue(momentValue) {
        const {isGregorian, timePicker} = this.state;

        if (this.props.onChange) {
            this.props.onChange(momentValue);
        }

        const inputValue = this.getValue(momentValue, isGregorian, timePicker);

        this.setState({momentValue, inputValue});
    }

    handleSelectDay(selectedDay) {
        const {momentValue: oldValue} = this.state;
        let momentValue = selectedDay.clone();

        if (oldValue) {
            momentValue = momentValue
                .set({
                    hour: oldValue.hours(),
                    minute: oldValue.minutes(),
                    second: oldValue.seconds()
                });
        }

        this.setMomentValue(momentValue);
    }

    start_at_onchange = (event) => {
        this.setState({
            start_at: toPersianDigits(event.target.value)
        });
    };

    render() {
        const {isGregorian, start_at} = this.state;
        const {
            min, max, selectedDay, max_month, min_month,
            defaultMonth, syncSelectedDay, styles, start_at_onChange,
            calendarContainerProps, nextMonth, prevMonth
        } = this.props;

        return (
            <Calendar
                min={min}
                max={max}
                min_month={min_month}
                max_month={max_month}
                selectedDay={selectedDay}
                defaultMonth={defaultMonth}
                onSelect={this.handleSelectDay.bind(this)}
                outsideClickIgnoreClass={outsideClickIgnoreClass}
                styles={styles}
                containerProps={calendarContainerProps}
                isGregorian={isGregorian}
                syncSelectedDay={syncSelectedDay}
                nextMonth={nextMonth}
                prevMonth={prevMonth}
                start_at={start_at}
                start_at_text_on_change={this.start_at_onchange}
                start_at_onChange={start_at_onChange}
            />
        );
    }

    removeDate() {
        const {onChange} = this.props;
        if (onChange) {
            onChange('');
        }
        this.setState({
            input: '',
            inputValue: ''
        });
    }
}


export default class RangePicker extends Component {
    state = {
        selectedDay: [],
        currentMonth: moment(),
    };

    start_at_onChange = (is_min_month) => {
        const {isGregorian} = this.state;
        const monthFormat = isGregorian ? 'Month' : 'jMonth';
        const dateFormat = isGregorian ? 'YYYY/M/D' : 'jYYYY/jM/jD';
        return (event) => {
            if (is_min_month) {
                const currentMonth = moment(toEnglishDigits(event.target.value), dateFormat);
                if (currentMonth.isValid()) {
                    this.setState({currentMonth});
                } else {
                    this.setState({currentMonth: moment()});
                }
            } else {
                const currentMonth = moment(toEnglishDigits(event.target.value), dateFormat);
                if (currentMonth.isValid()) {
                    this.setState({currentMonth: currentMonth.subtract(1, monthFormat)});
                } else {
                    this.setState({currentMonth: moment()});
                }
            }
        };
    };

    syncSelectedDay(state) {
        this.setState(state);
    }

    nextMonth() {
        const {isGregorian} = this.state;
        const monthFormat = isGregorian ? 'Month' : 'jMonth';

        this.setState({currentMonth: moment(this.state.currentMonth).add(1, monthFormat)});
    }

    prevMonth() {
        const {isGregorian} = this.state;
        const monthFormat = isGregorian ? 'Month' : 'jMonth';

        this.setState({currentMonth: moment(this.state.currentMonth).subtract(1, monthFormat)});
    }

    onClick = (days) => {
        return () => {
            return this.setState({
                selectedDay: [...days],
                currentMonth: days[0]
            });
        }
    };

    getRange = () => {
        const {getRange} = this.props;

        if(getRange) {
            getRange(this.state);
        }
    };

    dismiss = () => {

    };

    render() {
        const {selectedDay, currentMonth} = this.state;

        const {isGregorian} = this.state;
        const monthFormat = isGregorian ? 'Month' : 'jMonth';
        const nextMonthDate = currentMonth.clone().add(1, monthFormat);

        return (<div className="rangePicker">
            <div className="rangePickersWrapper">
                <div className="wrapRangePicker">
                    <DatePicker
                        nextMonth={this.nextMonth.bind(this)}
                        prevMonth={this.prevMonth.bind(this)}
                        syncSelectedDay={this.syncSelectedDay.bind(this)}
                        max_month={true}
                        selectedDay={selectedDay}
                        defaultMonth={nextMonthDate}
                        isGregorian={false}
                        key={Math.random()}
                        start_at_onChange={this.start_at_onChange(false)}
                    />

                    <DatePicker
                        nextMonth={this.nextMonth.bind(this)}
                        prevMonth={this.prevMonth.bind(this)}
                        syncSelectedDay={this.syncSelectedDay.bind(this)}
                        min_month={true}
                        isGregorian={false}
                        defaultMonth={currentMonth}
                        selectedDay={selectedDay}
                        key={Math.random()}
                        start_at_onChange={this.start_at_onChange(true)}
                    />
                </div>
                <div className="rangePickerFooter">
                    <div className="rangePickerButton" onClick={this.getRange}>ثبت</div>
                    <div className="rangePickerButton" onClick={this.dismiss}>انصراف</div>
                </div>
            </div>

            <div className="filters" key={Math.random()}>
                <div className="filter-header">میانبر سریع</div>
                <div className="date-filter">
                    <span onClick={this.onClick([moment()])}>امروز</span>
                </div>
                <div className="date-filter">
                    <span onClick={this.onClick([moment().subtract(1, 'days')])}>دیروز</span>
                </div>
                <div className="date-filter">
                    <span onClick={this.onClick([moment().subtract(6, 'days'), moment()])}>۷ روز گذشته</span>
                </div>
                <div className="date-filter">
                        <span
                            onClick={this.onClick([moment().startOf('jMonth'), moment().endOf("jMonth")])}>این ماه</span>
                </div>
                <div className="date-filter">
                    <span onClick={this.onClick([moment().subtract(1, 'jMonth'), moment()])}>ماه گذشته</span>
                </div>
                <div className="date-filter">
                    <span onClick={this.onClick([moment().subtract(2, 'jMonth').startOf('jMonth'), moment()])}>سه ماه اخیر</span>
                </div>
            </div>
        </div>)
    }
}