import React, {Component, PropTypes} from "react";
import {persianNumber} from "../utils/persian";
import {leftArrow, rightArrow} from "../utils/assets";

export default class Heading extends Component {
    static propTypes = {
        month: PropTypes.object.isRequired,
        isGregorian: PropTypes.bool
    };

    static contextTypes = {
        styles: PropTypes.object,
        setCalendarMode: PropTypes.func.isRequired
    };

    render() {
        const {month, styles, min_month, max_month, prevMonth, nextMonth} = this.props;

        return (
            <div className={styles.heading}>
                <button className={styles.title}>
                    {this.props.isGregorian ? month.locale('en').format('MMMM YYYY') : persianNumber(month.locale('fa').format('jMMMM jYYYY')).replace('امرداد', 'مرداد') }
                </button>
                {min_month && <button
                    type="button"
                    title="ماه قبل"
                    className={styles.prev}
                    onClick={prevMonth}
                    dangerouslySetInnerHTML={rightArrow}
                />}
                {max_month && <button
                    type="button"
                    title="ماه بعد"
                    className={styles.next}
                    onClick={nextMonth}
                    dangerouslySetInnerHTML={leftArrow}
                />}
            </div>
        );
    }
}
